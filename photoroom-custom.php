<?php
/**
 * Plugin Name:       Custom popup template design
 * Plugin URI:        https://cmsmart.net
 * Description:       Custom popup template design
 * Version:           1.0.1
 * Requires at least: 5.2
 * Requires PHP:      7.1
 * Author:            Huy
 * Author URI:        https://cmsmart.net
 * License:           GPL v2 or later
 * License URI:       https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain:       Custom popup template design
 * Domain Path:       /languages
 */


add_action('nbd_js_config','nbd_js_photoroom');
function nbd_js_photoroom(){
    ?>
        var bifold_js = 1;
    <?php 
}

add_action( 'wp_enqueue_scripts','add_js_ct_phottoroom',100);
function add_js_ct_phottoroom() {
    wp_add_inline_script( 'nbdesigner', 'const bifold_js = true;', 'before' );
    wp_enqueue_style('my-galery', plugin_dir_url(__FILE__) . 'assets/custom.css');
}

add_action('nbd_extra_css','custom_phottoroom_style');
function custom_phottoroom_style(){
    ?>
        <link rel="stylesheet" href="<?= plugin_dir_url(__FILE__) . 'assets/style.css'; ?>">
    <?php
}


add_filter('nbd_template_mapping_fields','nbd_template_mapping_fields_ft');
function nbd_template_mapping_fields_ft($fields) {
    $fields['custom_bi'] = array(
        "label" =>  esc_html__('Custom textarea', 'web-to-print-online-designer')
    );
    return $fields;
}

add_action('add_popup_test','nbod_cutomize_popup_tempalte_ft');

add_filter('nbod_cutomize_popup_tempalte','nbod_cutomize_popup_tempalte_ft');
function nbod_cutomize_popup_tempalte_ft() {
    ?>
        <div class="nbd-popup popup-template-fields" data-animate="scale">
            <div class="overlay-popup"></div>
            <div class="main-popup">
                <i class="icon-nbd icon-nbd-clear close-popup"></i>
                <div class="head">
                    <h2 style="text-align: center;"><?php esc_html_e('Your Information','web-to-print-online-designer'); ?></h2>
                </div>
                <div class="body">
                    <div class="main-body tab-scroll">
                        <div ng-repeat="field in templateHolderFields" class="md-input-wrap">
                            <textarea class="bd-ct" style="border: 1px solid #ddd;border-radius: 4px;padding-top: 20px;padding-left: 6px;" ng-model="field.value" ng-class="field.connect_to == 'custom_bi' ? '' : 'hideH'" rows="4" cols="50"></textarea>
                            <input id="fm-{{field.key}}" ng-class="field.connect_to == 'custom_bi' ? 'hideH' : ''" id="fm-{{field.key}}" ng-model="field.value" ng-class="field.value.length > 0 ? 'holder' : ''"/>
                            <label class="ct-lb" for="fm-{{field.key}}" >{{field.name}}<label/>
                        </div>
                    </div>
                </div>
                <div class="footer">
                    <div class="act-btn">
                        <span class="nbd-button" ng-click="updateTemplateFields()"><?php esc_html_e('See your information','web-to-print-online-designer'); ?></span>
                    </div>
                </div>
            </div>
        </div>
    <?php
    return '';
}

add_action('before_nbdesigner_frontend_container','before_nbdesigner_frontend_container_ft');
function before_nbdesigner_frontend_container_ft() {
    ?>
        <div class="nbd-popup ct ct-hide">
            <div class="overlay-popup"></div>
            <div class="main-popup ct">
                <i class="ct-close">
                    <svg width="22px" height="22px" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" clip-rule="evenodd" d="M6.7929 7.49998L1.14645 1.85353L1.85356 1.14642L7.50001 6.79287L13.1465 1.14642L13.8536 1.85353L8.20711 7.49998L13.8536 13.1464L13.1465 13.8535L7.50001 8.20708L1.85356 13.8535L1.14645 13.1464L6.7929 7.49998Z" fill="#000000"/>
                    </svg>
                </i>
                <div class="head">
                    <h2 class="head-bt_ct"><?php esc_html_e('Are you Sure ?','web-to-print-online-designer'); ?></h2>
                </div>
                <div class="body">
                    <div class="main-body bd-ct">
                        <p class="clr">Are you absolutely certain you wish to proceed?</p>
                        <p>By clicking the "Start Over" button, all progress on the template 
                            you've meticulously crafted will be irreversibly erased. 
                            Please take a moment to consider the implications of this action, 
                            as there will be no way to recover your previous design
                        </p>
                        <p>
                            We recommend saving a copy of your current template 
                            before proceeding if you have any doubts. 
                            Your decision to start over will result in the complete removal 
                            of your existing work
                        </p>
                        <p>
                            Please confirm your choice by clicking
                            "<span class="clr">Start Over</span>" again or press "<span class="clr">Cancel</span>" to preserve your current design.
                        </p>        
                        <p>Remember, once you proceed, there's no turning back.</p>
                    </div>
                    <div class="two-bt">
                        <button class="bg-black">Cancel</button>
                        <button class="bg-red">Start Over</button>
                    </div>
                </div>
            </div>
        </div> 
    <?php
}

